import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.util.concurrent.Callable;


/**
 * This class reads from server and writes the content to a file. If a byte is not received within timeout then connection closes
 * and process ends for the TCPClient
 */
class TimerTaskAscii implements Callable<String> {

    private DataOutputStream bufferedOutputStream;
    private BufferedReader inFromServer;

    TimerTaskAscii(DataOutputStream bufferedOutputStream, BufferedReader inFromServer) {
        this.bufferedOutputStream = bufferedOutputStream;
        this.inFromServer = inFromServer;
    }

    //To be executed with a time restriction
    @Override
    public String call() throws Exception {
        bufferedOutputStream.write(inFromServer.read());
        return "Ready";
    }
}
