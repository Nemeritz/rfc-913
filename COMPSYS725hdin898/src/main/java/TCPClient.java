/**
 * Hanliang Ding
 * upi: hdin898
 * id: 568689287
 **/

import javax.xml.crypto.Data;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.*;

class TCPClient {

    static String currentDir;
    static BufferedReader inFromUser;
    static Socket clientSocket;
    static DataOutputStream outToServer;
    static BufferedReader inFromServer;
    static long timerTime;
    static Types currentType = Types.Binary;
    static DataInputStream binaryFileInputStream;

    static public void main(String argv[]) throws Exception {

        //Initialization and connecting with the server
        Path currentRelativePath = Paths.get("");
        currentDir = currentRelativePath.toAbsolutePath().toString();
        Boolean LoggedIn = false;
        String userSentence;
        String stringFromServer = "";
        inFromUser = new BufferedReader(new InputStreamReader(System.in));
        clientSocket = new Socket("localhost", 6787);
        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        binaryFileInputStream = new DataInputStream(clientSocket.getInputStream());
        timerTime = 0;

        while (true) {

            userSentence = inFromUser.readLine();
            String userSentenceCode = userSentence.substring(0, 4);


            if (userSentence != null) {

                if (userSentence.length() < 4) {
                    System.out.println("Command invalid, minimum 4 character code");
                }

                //check if the file for STOR is valid or not
                else if (userSentenceCode.equals("STOR") && !new File(currentDir + "/" + userSentence.substring(9, userSentence.length())).isFile()) {
                    System.out.println("Invalid file, specify new file");
                }

                //check if server has returned a "!" yet or not. If not, only identification commands are allowed
                else if (!LoggedIn && !userSentenceCode.equals("USER") && !userSentenceCode.equals("ACCT")
                        && !userSentenceCode.equals("PASS") && !userSentenceCode.equals("DONE")) {
                    System.out.println("Not logged in, please login before sending other commands");
                }

                //otherwise send command
                else {
                    outToServer.writeBytes(userSentence + '\n');
                    stringFromServer = inFromServer.readLine();

                    //if there are lines left in the buffer then keep reading and concatenate them
                    while (inFromServer.ready() || stringFromServer != "") {
                        if (inFromServer.ready()) {
                            String temp = inFromServer.readLine();
                            stringFromServer += "\r\n" + temp;
                        }
                        else {
                            System.out.println("FROM SERVER: " + stringFromServer);
                            if (stringFromServer.substring(0, 1).equals("!")) {
                                LoggedIn = true;
                            }

                            //functions to handle RETR, DONE and STOR as they need extra actions from client side
                            switch (userSentenceCode) {
                                case "RETR": RETRresponse(userSentence, stringFromServer);
                                    userSentence = "";
                                    break;
                                case "DONE": DONEresponse(stringFromServer);
                                    userSentence = "";
                                    break;
                                case "STOR": STORresponse(userSentence, stringFromServer);
                                    break;
                                case "TYPE": TYPEresponse(userSentence, stringFromServer);
                                    break;
                                default:
                                    break;
                            }


                            stringFromServer = "";
                        }
                    }
                }
            }
        }
    }

    /**
     * RETRresponse function runs when client enters a RETR command, it checks for storage space and server responses
     * @param userSentence user's last typed & entered line
     * @param messageFromServer server's response
     */
    private static void RETRresponse(String userSentence, String messageFromServer) throws IOException, ExecutionException, InterruptedException {
        int fileSize;

        //check if server is ok with the file
        if (messageFromServer.substring(0, 1).equals("-")) {
            return;
        }

        fileSize = Integer.parseInt(messageFromServer);

        //check if there's enough storage space
        if (fileSize > Files.getFileStore(new File(currentDir).toPath()).getUsableSpace()) {
            outToServer.writeBytes("STOP");
            return;
        }

        //tells the server that client has enough space to receive file
        outToServer.writeBytes("SEND" + "\n");
        System.out.println("receiving file...");

        String filename = userSentence.substring(5, userSentence.length());
        saveFile(filename, fileSize);
    }

    /**
     * STORresponse function checks for the validity of the file and makes sure the server has enough
     * storage space when user enters STOR command
     * @param userSentence user's last entered line
     * @param messageFromServer server's response to the STOR command
     */
    private static void STORresponse(String userSentence, String messageFromServer) throws IOException {

        File fileToSend = new File(currentDir + "/" + userSentence.substring(9, userSentence.length()));

        if (!fileToSend.exists() || fileToSend.isDirectory()) {
            System.out.println("invalid file");
            return;
        }

        if (messageFromServer.substring(0, 1).equals("-")) {
            return;
        }

        outToServer.writeBytes("SIZE " + fileToSend.length() + "\n");

        messageFromServer = inFromServer.readLine();
        System.out.println("FROM SERVER: " + messageFromServer);

        if (messageFromServer.substring(0, 1).equals("-")) {
            return;
        }

        sendFileToServer(fileToSend);
    }

    /**
     * DONEresponse closes the connectionSocket if server returns "+" to end
     * @param messageFromServer messageFromServer received from server
     */
    private static void DONEresponse(String messageFromServer) throws IOException {
        if (messageFromServer.substring(0, 1).equals("+")) {
            clientSocket.close();
            System.exit(0);
        }
    }

    /**
     * STORresponse function checks for the validity of the file and makes sure the server has enough
     * storage space when user enters STOR command
     * @param userSentence user's last entered line
     * @param messageFromServer server's response to the STOR command
     */
    private static void TYPEresponse(String userSentence, String messageFromServer) throws IOException {

        if (messageFromServer.substring(0, 1).equals("-")) {
            return;
        }

        switch (userSentence.substring(5, userSentence.length())) {
            case "A":
                currentType = Types.Ascii;
                break;
            case "B":
                currentType = Types.Binary;
                break;
            case "C":
                currentType = Types.Continuous;
                break;
            default:
                break;
        }
    }

    /**
     * saveFile saves a file to the current default directory, when a byte has not been received for 2 seconds then
     * end process and close connection socket
     * @param fileName name of the file to save as a string
     * @param fileSize size of the file to store, received from server
     */
    private static void saveFile(String fileName, int fileSize) throws IOException, ExecutionException, InterruptedException {
        File file = new File(currentDir + "/" + fileName);
        FileOutputStream fileOutStream = new FileOutputStream(file);
        DataOutputStream bufferedOutStream = new DataOutputStream(fileOutStream);

        //Binary and Continuous modes, this can read binary files
        if (currentType == Types.Binary || currentType == Types.Continuous) {

            // Read and write for all bytes
            for (int i = 0; i < fileSize; i++) {

                System.out.println(i);
                System.out.println(inFromServer.ready());
                //initialize and start timeout timer
                ExecutorService executor = Executors.newSingleThreadExecutor();
                Future<String> future = executor.submit(new TimerTaskBinary(bufferedOutStream, binaryFileInputStream));

                try {
                    //This is the object that is reading and storing the file
                    future.get(2, TimeUnit.SECONDS);
                } catch (TimeoutException e) {
                    //if timeout exceeded then close socket and end process
                    outToServer.writeBytes("DONE" + "\n");
                    System.out.println("Transfer timeout, closing socket");
                    clientSocket.close();
                    System.exit(0);
                    future.cancel(true);
                }

                executor.shutdownNow();
            }

            bufferedOutStream.close();
            fileOutStream.close();
            System.out.println("finished receiving file.");
        }

        //Ascii mode, uses BufferedReader for reading which will corrupt binary files. But this is checked on server side
        else if (currentType == Types.Ascii) {
            // Read and write for all bytes
            for (int i = 0; i < fileSize; i++) {

                System.out.println(i);
                System.out.println(inFromServer.ready());
                //initialize and start timeout timer
                ExecutorService executor = Executors.newSingleThreadExecutor();
                Future<String> future = executor.submit(new TimerTaskAscii(bufferedOutStream, inFromServer));

                try {
                    //This is the object that is reading and storing the file
                    future.get(2, TimeUnit.SECONDS);
                } catch (TimeoutException e) {
                    //if timeout exceeded then close socket and end process
                    outToServer.writeBytes("DONE" + "\n");
                    System.out.println("Transfer timeout, closing socket");
                    clientSocket.close();
                    System.exit(0);
                    future.cancel(true);
                }

                executor.shutdownNow();
            }

            bufferedOutStream.close();
            fileOutStream.close();
            System.out.println("finished receiving file.");
        }
    }

    /**
     * sendFileToServer function sends the file specified by the user to the server in bytes
     * @param fileToSend the file to send to server
     */
    private static void sendFileToServer(File fileToSend) throws IOException {
        byte[] bytes = new byte[(int) fileToSend.length()];

        try {
            FileInputStream fis = new FileInputStream(fileToSend);
            DataInputStream bufferedInStream = new DataInputStream(new FileInputStream(fileToSend));

            int content = 0;

            // Read and send file until the whole file has been sent
            while ((content = bufferedInStream.read(bytes)) >= 0) {
                outToServer.write(bytes, 0, content);
            }

            bufferedInStream.close();
            fis.close();
            outToServer.flush();
        } catch (FileNotFoundException e) {
            System.out.println("Error, file not found");
            return;
        } catch (IOException e) {
            System.out.println("Error, IO exception");
            return;
        }

        String messageFromServer = inFromServer.readLine();
        System.out.println("FROM SERVER: " + messageFromServer);
    }
}

