/**
 * Code is taken from Computer Networking: A Top-Down Approach Featuring 
 * the Internet, second edition, copyright 1996-2002 J.F Kurose and K.W. Ross, 
 * All Rights Reserved.
 **/

import java.net.*;

/**
 * Creates a TCPConnectionInstance object everytime a new client connection socket appears
 * and runs it on a new thread
 */
class TCPServer {

    public static void main(String argv[]) throws Exception {
		ServerSocket welcomeSocket = new ServerSocket(6787);

		while (true) {

			Socket connectionSocket = welcomeSocket.accept();

			new Thread(new TCPConnectionInstance(connectionSocket)).start();
		}
	}
} 

