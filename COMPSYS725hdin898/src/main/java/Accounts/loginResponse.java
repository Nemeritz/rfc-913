package Accounts;

/**
 * Enum that shows whether an operation with the database was successful or not, not necessarily logging in which
 * may be misleading. SUCCESS is success, failed means failed or invalid, existing means already logged in.
 */
public enum loginResponse {
    SUCCESS, FAILED, EXISTING
}
