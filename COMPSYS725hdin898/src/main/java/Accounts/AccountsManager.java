package Accounts;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This class manages the account.txt file which is the database for the server
 */
public class AccountsManager {

    private List<String[]> accountsLines;
    private String accountsDir;

    /**
     * finds the database txt file in the Accounts folder and stores each user in an arraylist
     */
    public AccountsManager() throws IOException {
        //this is the variable which stores the fields of each entry in the database
        accountsLines = new ArrayList<String[]>();

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        accountsDir = s+"/Accounts/Accounts.txt";
        BufferedReader readerAccounts = new BufferedReader(new FileReader(accountsDir));
        String line = readerAccounts.readLine();

        //splits each field by ","
        while (line != null) {
            accountsLines.add(line.split(","));
            line = readerAccounts.readLine();
        }
    }

    /**
     * isValidUserID checks for the validity of the USERID and sees if its already logged in or not,
     * It's synchronized so that other instances cannot change the database while it's accessing it
     * @param ID the USER ID to be tested
     * @return loginResponse SUCCESS if exists, EXISTING if already logged in, FAILED if invalid
     */
    synchronized public loginResponse isValidUserID(String ID) throws IOException {
        //updates the accountLines variable so that the function's version of accounts is up to date with the database
        updateAccountLines();

        if (ID != null) {
            //checks all the lines in the file and sees if any USERID corresponding to ID exists
            for (int i = 0; i < accountsLines.size(); i++) {
                if (accountsLines.get(i)[0].equals(ID)) {
                    //checks if already logged in
                    if (accountsLines.get(i)[3].equals("yes")) {
                        return loginResponse.EXISTING;
                    }
                    return loginResponse.SUCCESS;
                }
            }
        }

        return loginResponse.FAILED;
    }

    /**
     * isValidUsername checks for the validity of the username, logic same as isValidUserID function.
     * @param username the Account username to be tested
     * @return loginResponse SUCCESS if exists, EXISTING if already logged in, FAILED if invalid
     */
    synchronized public loginResponse isValidUsername(String username) throws IOException {

        updateAccountLines();

        for (int i = 0; i < accountsLines.size(); i++) {
            if (accountsLines.get(i)[1].equals(username)) {
                if (accountsLines.get(i)[3].equals("yes")) {
                    return loginResponse.EXISTING;
                }
                return loginResponse.SUCCESS;
            }
        }
        return loginResponse.FAILED;
    }

    /**
     * isValidPassword checks for the validity of the username, logic same as isValidUserID function.
     * @param password the Account password to be tested
     * @return loginResponse SUCCESS if exists, EXISTING if already logged in, FAILED if invalid
     */
    synchronized public loginResponse isValidPassword(String password) throws IOException {

        updateAccountLines();

        for (int i = 0; i < accountsLines.size(); i++) {
            if (accountsLines.get(i)[2].equals(password)) {
                if (accountsLines.get(i)[3].equals("yes")) {
                    return loginResponse.EXISTING;
                }
                return loginResponse.SUCCESS;
            }
        }
        return loginResponse.FAILED;
    }

    /**
     * isAdmin checks for the validity of the username, logic same as isValidUserID function.
     * @param username the Account password to be tested
     * @return loginResponse SUCCESS if exists, EXISTING if already logged in, FAILED if invalid
     */
    synchronized public boolean isAdmin(String username, String password) throws IOException {

        updateAccountLines();

        for (int i = 0; i < accountsLines.size(); i++) {
            if (accountsLines.get(i)[1].equals(username)) {
                if (accountsLines.get(i)[2].equals(password)) {
                    if (accountsLines.get(i)[4].equals("admin")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * tryLogin tries to login with the specified username and password, synchronized again to prevent data corruption
     * @param username the Account username to be used
     * @param password the Account password to be used
     * @return loginResponse SUCCESS if successfully logged in, FAILED if failed to login
     */
    synchronized public loginResponse tryLogin(String username, String password) throws IOException {

        //updates the database
        updateAccountLines();

        for (int i = 0; i < accountsLines.size(); i++) {
            //finds the username
            if (accountsLines.get(i)[1].equals(username)) {
                //checks if password is the same as password in database
                if (accountsLines.get(i)[2].equals(password)) {
                    try {
                        //changes the logged in field in the database
                        accountsLines.get(i)[3] = ("yes");
                        File file = new File(accountsDir);
                        Files.write(file.toPath(), toStringList());
                        updateAccountLines(); // updates the accountLines variable so the instance is up to date
                        return loginResponse.SUCCESS;
                    }
                    catch (IOException e) {
                        return loginResponse.FAILED;
                    }
                }
            }
        }
        return loginResponse.FAILED;
    }

    /**
     * logOff tries to log off the account associated with the USER ID
     * @param UserID the user to be logged off
     * @return loginResponse SUCCESS if successfully logged off, FAILED if failed to logoff
     */
    synchronized public loginResponse logOff(String UserID) throws IOException {

        updateAccountLines();

        for (int i = 0; i < accountsLines.size(); i++) {
            //looks for the entry with the same USERID in the database
            if (accountsLines.get(i)[0].equals(UserID)) {
                try {
                    //changes the logged in status to no
                    accountsLines.get(i)[3] = ("no");
                    File file = new File(accountsDir);
                    Files.write(file.toPath(), toStringList());
                    return loginResponse.SUCCESS;
                }
                catch (IOException e) {
                    return loginResponse.FAILED;
                }
            }
        }
        return loginResponse.FAILED;
    }

    /**
     * toStringList merges the accountLines' elements with ',' so it can be written as a single line into the database
     * @return converts the list of strings to write to the database
     */
    private List<String> toStringList() {
        List<String> mergedLines = new ArrayList<>();
        for (int i = 0; i < accountsLines.size(); i++) {
            mergedLines.add(String.join(",", accountsLines.get(i)));
        }
        return mergedLines;
    }


    /**
     * updateAccountLines function updates the accountLines variable with the database, synchronizes the database and the field
     */
    private void updateAccountLines() throws IOException {
        accountsLines.clear();
        BufferedReader readerAccounts = new BufferedReader(new FileReader(accountsDir));
        String line = readerAccounts.readLine();

        while (line != null) {
            accountsLines.add(line.split(","));
            line = readerAccounts.readLine();
        }
    }
}
