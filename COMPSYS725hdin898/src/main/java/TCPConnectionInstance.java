import Accounts.AccountsManager;
import Accounts.loginResponse;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class TCPConnectionInstance implements Runnable {


    //SET THIS TO TRUE to introduce an artificial delay in RETR so the timeout in client side can be tested
    private boolean DEBUGTIMEOUT = false;


    private volatile Boolean ThreadRunning = true;
    private String UserID = "";
    private String Username = "";
    private String Password = "";
    private String adminUsername = "";
    private String adminPassword = "";
    private String storeType = "";
    private Types currentType = Types.Binary;
    private String currentDir;
    private AccountsManager am;
    private Socket connectionSocket;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    private String cdir;
    private File oldFile;
    private File fileGet;
    private File fileStore;

    /**
     * Constructor for a new client connection
     * @param connectionSocket the connection socket for TCP. communicates using this socket
     */
    public TCPConnectionInstance(Socket connectionSocket) throws IOException {
        this.am = new AccountsManager();
        this.connectionSocket = connectionSocket;
        this.inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        this.outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        this.cdir = "";
        Path currentRelativePath = Paths.get("");
        this.currentDir = currentRelativePath.toAbsolutePath().toString();
    }

    /**
     * run Keeps the thread running if DONE was not received and check for client commands forever
     */
    public void run() {
        while (ThreadRunning) {
            try {
                String clientPacket;
                clientPacket = inFromClient.readLine();
                if (clientPacket != null) {
                    response(outToClient, clientPacket);
                }
            } catch (SocketException e) {
                //ends thread if the client closes connection. This will happen when RETR timeout on client side
                ThreadRunning = false;
                System.out.println("Socket closed, ending thread");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * response function Handles all the commands that the client can send, switch is self explanatory
     * @param dos dataoutputstream to the client socket
     * @param clientPacket the message/command that the client send
     */
    synchronized public void response(DataOutputStream dos, String clientPacket) throws IOException {
        if (clientPacket != null) {

            String argumentFromClient = "";

            //checks if the command is one with arguments or not
            if (clientPacket.length() > 4) {
                argumentFromClient = clientPacket.substring(5, clientPacket.length());
            }

            switch (clientPacket.substring(0, 4)) {
                case "USER":
                    if (!argumentFromClient.equals("")) {
                        dos.writeBytes(USERresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "ACCT":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(ACCTresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "PASS":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(PASSresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "TYPE":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(TYPEresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "LIST":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(LISTresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "CDIR":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(CDIRresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "KILL":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(KILLresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "NAME":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(NAMEresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "TOBE":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(TOBEresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "RETR":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(RETRresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "SIZE":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(SIZEresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "STOP":
                    dos.writeBytes(STOPresponse() + "\n");
                    break;
                case "SEND":
                    SENDresponse();
                    break;
                case "STOR":
                    if (!argumentFromClient.equals("")) {
                    dos.writeBytes(STORresponse(argumentFromClient) + "\n");
                    }
                    else {
                        dos.writeBytes("-missing argument" + "\n");
                    }
                    break;
                case "DONE":
                    //closes the connection and ends the thread, also clears the login of the user
                    dos.writeBytes(DONEresponse() + "\n");
                    connectionSocket.close();
                    am.logOff(UserID);
                    ThreadRunning = false;
                    break;
                default:
                    dos.writeBytes("Invalid CODE \n");
                    break;
            }
        }
    }

    /**
     * USERresponse function checks for the validity of the USER ID sent by the client
     * and if the user is already logged in or not
     * @param argumentFromClient the USER ID sent by the client
     * @return response to the client
     */
    public String USERresponse(String argumentFromClient) throws IOException {

        //checks the validity of the USER ID sent by the client
        loginResponse lr = am.isValidUserID(argumentFromClient);

        //stpres the user ID unless it's invalid
        if (lr == loginResponse.SUCCESS) {
            this.UserID = argumentFromClient;
            return "+User-id valid, send account and password";
        }
        else if (lr == loginResponse.EXISTING) {
            this.UserID = argumentFromClient;
            return "!" + argumentFromClient + " logged in";
        }
        else {
            return "-Invalid user-id, try again";
        }
    }

    /**
     * ACCTresponse function checks for the validity of the account name sent by the client
     * and if the user is already logged in or not. If it's valid and password already given then try login
     * @param argumentFromClient the Account name sent by the client
     * @return response to the client
     */
    public String ACCTresponse(String argumentFromClient) throws IOException {

        //not currently in cdir mode asking for admin access
        if (cdir.equals("")) {
            loginResponse lr = this.am.isValidUsername(argumentFromClient);

            // checks if valid and not already logged in
            if (lr == loginResponse.SUCCESS) {
                this.Username = argumentFromClient;
                if (this.Password.equals("")) {
                    return "+" + argumentFromClient + " valid, send password";
                }
                //try to login if password already given
                else {
                    if (this.am.tryLogin(argumentFromClient, this.Password) == loginResponse.SUCCESS) {
                        this.Username = argumentFromClient;
                        return "! " + argumentFromClient + " valid, logged-in";
                    } else {
                        return "-Invalid account, try again";
                   }
                }
            } else if (lr == loginResponse.EXISTING) {
                this.Username = argumentFromClient;
                return "! " + argumentFromClient + " valid, logged-in";
            } else {
                return "-Invalid account, try again";
            }
        }
        //in cdir mode and only take admin access
        else {
            if (adminPassword != "" && am.isAdmin(argumentFromClient, adminPassword)) {
                adminPassword = "";
                String tempcdir = cdir;
                currentDir = cdir;
                cdir = "";
                return "!Admin account verified, Changed working dir to " + tempcdir;
            } else if (adminPassword == "") {
                adminUsername = argumentFromClient;
                return "+correct username, please send password for potential admin access";
            } else {
                adminUsername = "";
                adminPassword = "";
                return "-correct credentials but not admin access, please send account/username of admin";
            }
        }
    }

    /**
     * PASSresponse function checks for the validity of the password sent by the client
     * and if the user is already logged in or not. If it's valid and account name already given then try login
     * @param argumentFromClient the password sent by the client
     * @return response to the client
     */
    public String PASSresponse(String argumentFromClient) throws IOException {

        //not currently in cdir mode asking for admin access
        if (cdir.equals("")) {
            loginResponse lr = this.am.isValidPassword(argumentFromClient);

            // checks if valid and not already logged in
            if (lr == loginResponse.SUCCESS) {
                this.Password = argumentFromClient;
                if (this.Username.equals("")) {
                    return "+ valid password, send username";
                }
                //try to login if password already given
                else {
                    if (this.am.tryLogin(this.Username, argumentFromClient) == loginResponse.SUCCESS) {
                        this.Password = argumentFromClient;
                        return "!valid password, logged-in";
                    } else {
                        return "-Invalid password, try again";
                    }
                }
            } else if (lr == loginResponse.EXISTING) {
                this.Password = argumentFromClient;
                return "!valid password, logged-in";
            } else {
                return "-Invalid password, try again";
            }
        }
        //in cdir mode and only take admin access
        else {
            if (adminUsername != "" && am.isAdmin(adminUsername, argumentFromClient)) {
                adminUsername = "";
                String tempcdir = cdir;
                currentDir = cdir;
                cdir = "";
                return "!Admin account verified, Changed working dir to " + tempcdir;
            } else if (adminUsername == "") {
                adminPassword = argumentFromClient;
                return "+correct password, please send username for potential admin access";
            } else {
                adminUsername = "";
                adminPassword = "";
                return "-correct credentials but not admin access, please send account/username of admin";
            }
        }
    }

    /**
     * TYPEresponse function changes type according to the argument
     * @param argumentFromClient the type specified by the client
     * @return response to the client
     */
    public String TYPEresponse(String argumentFromClient) {

        String response;

        switch (argumentFromClient) {
            case "A":
                currentType = Types.Ascii;
                response = "+Using Ascii mode";
                break;
            case "B":
                currentType = Types.Binary;
                response = "+Using Binary mode";
                break;
            case "C":
                currentType = Types.Continuous;
                response = "+Using Continuous mode";
                break;
            default:
                response = "-Type not valid";
                break;
        }

        return response;
    }

    /**
     * LISTresponse lists the files in the default current directory if no directory specified, otherwise lists the directory specified
     * @param argumentFromClient the list type and directory specified by the client
     * @return response to the client
     */
    public String LISTresponse(String argumentFromClient) {

        String response;
        String listType = argumentFromClient.substring(0, 1);
        String path;

        // if path is not specified then set the path to current default directory
        try {
            path = argumentFromClient.substring(2);
        }
        catch (IndexOutOfBoundsException e) {
            path = currentDir;
        }


        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        //checks to list with standard or verbose, if neither then respond with error
        if (listType.equals("F") || listType.equals("V")) {
            response = "+" + path + ": " + UserID + " \r\n";
        }
        else {
            return "-invalid listing type";
        }

        //lists the files according to list type
        if (new File(path).exists()) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listType.equals("F")) {
                    if (i == listOfFiles.length - 1) {
                        response = response.concat(listOfFiles[i].getName());
                    }
                    else {
                        response = response.concat(listOfFiles[i].getName() + "\r\n");
                    }
                }
                else {
                    response = response.concat(String.format("%-30s %-20s %10s", listOfFiles[i].getName(), listOfFiles[i].length(),  new Date(listOfFiles[i].lastModified())));
                    response = response.concat("\r\n");
                }
            }
        }
        else {
            response = "-invalid directory";
        }

        return response;
    }

    /**
     * CDIRresponse Tries to change the current directory. If current logged in account is admin then change currentDir if not then ask for
     * admin username and password and stores the argument temporarily in cdir variable
     * @param argumentFromClient the directory specfied by the user, if none then it should be passed in as "" by the response function
     * @return response to the client
     */
    public String CDIRresponse (String argumentFromClient) throws IOException {

        loginResponse lr = am.isValidUserID(UserID);
        boolean isAdmin = am.isAdmin(Username, Password);

        //checks if the directory is valid
        if(new File(argumentFromClient).isDirectory()) {
            //if already logged in then change current directory
            if (isAdmin) {
                currentDir = argumentFromClient;
                return "!Changed working dir to " + argumentFromClient;
            }
            //if not logged in then ask for identification and store the argument in cdir
            else {
                cdir = argumentFromClient;
                return "+directory ok, send admin account/password";
            }
        }
        else {
            return "-invalid new directory";
        }
    }

    /**
     * KILLresponse deletes the file in the current directory if it exists
     * @param argumentFromClient the file to delete specfied by the user
     * @return response to the client
     */
    public String KILLresponse(String argumentFromClient) {
        File fileToKill = new File(currentDir + "/" + argumentFromClient);

        System.out.println(currentDir + "/" + argumentFromClient);

        if (fileToKill.isFile()) {
            fileToKill.delete();
            return "+" + argumentFromClient + " deleted";
        }
        else if (fileToKill.isDirectory()) {
            deleteFolder(fileToKill);
            return "+" + argumentFromClient + " deleted";
        }
        return "-Not deleted because file not found";
    }

    /**
     * NAMEresponse checks the validity of the file the user requested to rename
     * @param argumentFromClient the file to rename
     * @return response to the client
     */
    public String NAMEresponse(String argumentFromClient) {
        File fileToRename = new File(currentDir + "/" + argumentFromClient);

        if (fileToRename.exists()) {
            oldFile = fileToRename;
            return "+File exists";
        }
        return "-Can't find " + argumentFromClient + " NAME command is aborted, don't send TOBE";
    }

    /**
     * TOBEresponse checks the validity of the new name of the file the client wanted to rename, if valid then rename
     * @param argumentFromClient the new name of the file
     * @return response to the client
     */
    public String TOBEresponse(String argumentFromClient) {
        //checks a successful NAME command was received prior to this
        if (oldFile == null) {
            return "-File wasn’t renamed because old file not specified";
        }

        //new file name already exists, send another new name
        if (new File(currentDir + "/" + argumentFromClient).exists()) {
            return "-File wasn't renamed because already exists, use LIST command to check for valid file names";
        }

        //checks if rename was successful
        String oldName = oldFile.getName();
        if (oldFile.renameTo(new File(currentDir + "/" + argumentFromClient))) {
            oldFile = null;
            return "+" + oldName + " renamed to " + argumentFromClient;
        }

        return "-File wasn’t renamed because invalid new name";
    }

    /**
     * RETRresponse checks for the validity of the file the client wants and sends the size of the file
     * @param argumentFromClient file name of the file client wants
     * @return response to the client
     */
    public String RETRresponse(String argumentFromClient) {
        File fileToRetrieve = new File(currentDir + "/" + argumentFromClient);

        if (fileToRetrieve.isFile()) {
            if (currentType == Types.Ascii && !getFileType(fileToRetrieve).equals("text")) {
                return "-Cannot transfer binary file in ASCII mode, please use TYPE command to change";
            }
            else {
                fileGet = fileToRetrieve;
                return Long.toString(fileToRetrieve.length()) ;
            }
        }
        return "-File doesn't exist or is directory";
    }

    /**
     * SENDresponse is called when client has acknowledged that there is enough space to receive the file
     * This function sends the file in bytes
     * @return response to the client
     */
    public String SENDresponse() {
        //byte array of the files to send
        byte[] bytes = new byte[(int) fileGet.length()];

        try {
            FileInputStream fis = new FileInputStream(fileGet);
            DataInputStream bufferedInStream = new DataInputStream(new FileInputStream(fileGet));

            int content = 0;
            int byteRead = 0;
            // Read and send the file specified until the whole file has been sent
            while (bufferedInStream.available() > 0) {
                if (DEBUGTIMEOUT) {
                    //makes the thread sleep for 4 seconds so the client side time out
                    Thread.sleep(4000);
                }
                else {
                    byteRead = bufferedInStream.read();
                    outToClient.writeByte(byteRead);
                }
            }
            bufferedInStream.close();
            fis.close();
            outToClient.flush();
            fileGet = null;
        } catch (FileNotFoundException e) {
            return "-File not found";
        } catch (IOException e) {
            return "-File transfer terminated - IO exception";
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Finished Sending";
    }

    /**
     * STOPresponse clears the previously stored file that was to be retrieved
     * @return tells the client that RETR is aborted
     */
    public String STOPresponse() {
        fileGet = null;
        return "+ok, RETR aborted";
    }

    /**
     * STORresponse checks for the storing method that the client wanted and stores the file in fileStore variable
     * for when the client is ready to send. storeType and fileStore are both global variables
     * @param argumentFromClient name of the file and the storage type
     * @return response to client
     */
    public String STORresponse(String argumentFromClient) {
        String operationType = argumentFromClient.substring(0, 3);
        File fileToSend = new File(currentDir + "/" + argumentFromClient.substring(4));
        storeType = operationType;

        //if ascii and binary then respond with error message
        if (!getFileType(fileToSend).equals("text") && currentType == Types.Ascii) {
            return "-Please switch out of Ascii transfer type before sendning binary files";
        }

        //checks for the store type to be valid. If type is new and already file exists then return error
        if (operationType.equals("NEW")) {
            if (fileToSend.exists()) {
                //clears the fileStore so that SIZEreponse knows that STOR command did not succeed
                fileStore = null;
                return "-File exists, but system doesn't support generations";
            }
            else {
                fileStore = fileToSend;
                return "+File does not exist, will create new file";
            }
        }
        else if (operationType.equals("OLD")) {
            fileStore = fileToSend;
            if (fileToSend.exists()) {
                return "+Will write over old file";
            }
            else {
                return "+will create new file";
            }
        }
        else if (operationType.equals("APP")) {
            fileStore = fileToSend;
            if (fileToSend.exists()) {
                return "+Will append to file";
            }
            else {
                return "+will create file";
            }
        }
        //clears the storeType so that SIZEresponse knows that the STOR command did not succeed
        else {
            storeType = "";
            return "-invalid storage type";
        }
    }


    /**
     * SIZEresponse checks that there is enough room to store the file and stores it if there iss
     * @param argumentFromClient the size of the file to be sent
     * @return response to client
     */
    public String SIZEresponse(String argumentFromClient) {

        Path currentPath = new File(currentDir).toPath();
        boolean overwrite = true;

        if (fileStore == null || storeType == "") {
            return "-send successful STOR command with store type and file name first please";
        }

        String fileName = fileStore.getName();

        //if the storeType is old then overwrite file, otherwise append to file.
        if (storeType.equals("OLD")) {
            overwrite = false;
        }

        try {
            if (Integer.parseInt(argumentFromClient) < Files.getFileStore(currentPath).getUsableSpace()) {

                int fileSize = Integer.parseInt(argumentFromClient);

                outToClient.writeBytes("+ok, waiting for file" + "\n");

                //if file doesn't exist then create the new file
                if (!fileStore.isFile()) {
                    fileStore.createNewFile();
                }

                FileOutputStream fileOutStream = new FileOutputStream(fileStore, overwrite);
                BufferedOutputStream bufferedOutStream = new BufferedOutputStream(fileOutStream);
                DataOutputStream dataOutStream = new DataOutputStream(fileOutStream);
                DataInputStream dataInStream = new DataInputStream(connectionSocket.getInputStream());

                if (currentType == Types.Ascii) {

                    // Read and stores all the bytes into the file specified
                    for (int i = 0; i < fileSize; i++) {
                        bufferedOutStream.write(inFromClient.read());
                    }

                    fileStore = null;
                    bufferedOutStream.close();
                    fileOutStream.close();
                }
                else {
                    // Read and stores all the bytes into the file specified
                    for (int i = 0; i < fileSize; i++) {
                        dataOutStream.write(dataInStream.read());
                    }

                    fileStore = null;
                    dataOutStream.close();
                    fileOutStream.close();
                }
            }
            else {
                return "-Not enough room, don't send it";
            }

            return "+Saved " + fileName;
        }
        catch (IOException e) {
            return "-Error: IO Exception";
        }
        //checks client sent a valid number for file size
        catch (NumberFormatException e) {
            return "-file size format error";
        }
    }

    /**
     * DONEresponse tells the client that connection is closed
     * @return tells client that the connection is closed
     */
    private String DONEresponse() throws IOException {
        return "+closing connection";
    }

    /**
     * Recursively delete a folder and all folders/files in the folder
     * @param element the folder to delete
     */
    public void deleteFolder(File element) {
        if (element.isDirectory() && element.listFiles() != null) {
            for (File sub : element.listFiles()) {
                deleteFolder(sub);
            }
        }
        element.delete();
    }

    private String getFileType(File fileToCheck) {
        String mimetype= new MimetypesFileTypeMap().getContentType(fileToCheck);
        String type = mimetype.split("/")[0];
        System.out.println(type);
        return type;
    }
}
