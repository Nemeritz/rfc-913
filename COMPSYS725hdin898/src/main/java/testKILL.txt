KILL <filename>

Deletes the specified file in the current directory of the server.
We will do 3 tests:
1. Try delete the textKILL.txt file

    login with USER: AAA
    Make sure there is a testKILL.txt file in the src folder, if there isn't then create one. (it should be there by default)
    you can delete this file with the following KILL command
    you can verify that the file has been deleted by checking in the src folder again

    KILL testKILL.txt
    FROM SERVER: +testKILL.txt deleted

2. Try delete an invalid file

    login with USER: AAA
    KILL a non-existent file like below

    KILL randomFile.txt
    FROM SERVER: -Not deleted because file not found

3. Try delete a non-empty directory

    login with USER: AAA
    Make sure there is a folder called testKILLfolder and there are contents inside the folder (it should be there by default)
    KILL a directory recursively like below

    KILL testKILLfolder
    FROM SERVER: +testKILLfolder deleted

