# RFC-913

Hanliang Ding - hdin898 - 568689287

Program is case sensitive so when sending commands make sure the commands are all in capital letters.

# Run Instructions

1. Download the project
2. Copy the project in two different directories
3. Open terminal in each directory
4. In one terminal type 
javac TCPServer.java
5. In the same terminal type 
java TCPServer
6. In the other terminal type
javac TCPClient.java
7. In same terminal type
java TCPClient
### important: 8. enter DONE in client terminal before shutting down terminal otherwise database will not change logged in status of account that was logged in

# pre-loaded accounts in the Accounts.txt file (database)

1.Each account is a line in the file, the following format must be followed:

```
	USERID,AccountUsername,AccountPassword,no,notAdmin
```

2.the second last field corresponds to whether the account is currently logged in or not. If the account name or password is not needed then the following format is required:

```
	USERID,,,yes,notAdmin
```

An user with no username or password should always stay logged in so "yes" by default as they do not need to log in. As soon as they provide user id they get logged in.

3.the last field corresponds to whether the account is admin or not. This is needed when determining access in the CDIR command:

```
	USERID,,,yes,notAdmin
```


The current accounts in the database are:


```
	USERID: Chris
	AccountUsername: hdin898
	AccountPassword: 12345
	AdminAccess: no

	USERID: Billy
	AccountUsername: bdin898
	AccountPassword: 54321
	AdminAccess: no

	USERID: AAA
	AdminAccess: no
	
	USERID: Admin
	AccountUsername: adminA
	AccountPassword: adminP
	AdminAccess: yes
```
# COMMANDS

# USER <User ID>

User ID that you want to be logged in with

##### We will test 3 cases:

##### 1. valid USER ID with account and password
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
```

##### 2. valid USER ID with no account and password, this is the same response as an account that has already logged in
```
	USER AAA
	FROM SERVER: !AAA logged in
```

##### 3. invalid USER ID
```
	USER WrongUser
	FROM SERVER: -Invalid user-id, try again
```

# ACCT <username>

Account that you want to be logged in with

##### we will test 3 cases:

##### 1. valid account username
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
	ACCT hdin898
	FROM SERVER: +hdin898 valid, send password
```

##### 2. invalid account username
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
	ACCT WrongUser
	FROM SERVER: -Invalid account, try again
```

##### 3. valid account username which is already logged in, log in first like below and try ACCT again with the same username
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
	ACCT hdin898
	FROM SERVER: +hdin898 valid, send password
	PASS 12345
	FROM SERVER: ! Logged in
	ACCT hdin898
	FROM SERVER: ! hdin898 valid, logged-in
```

# PASS <password>

Password that you want to be logged in with

##### we will test 3 cases:

##### 1. valid password no account yet
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
	PASS 12345
	FROM SERVER: +Send account
```

##### 2. invalid password
```
	USER Chris
	FROM SERVER: !Chris logged in
	PASS WrongPassword
	FROM SERVER: -Wrong password, try again
```

##### 3. valid and already account username supplied + response when already logged in
```
	USER Chris
	FROM SERVER: +User-id valid, send account and password
	ACCT hdin898
	FROM SERVER: +hdin898 valid, send password
	PASS 12345
	FROM SERVER: ! Logged in
	PASS 12345
	FROM SERVER: ! Logged in
```

# TYPE { A | B | C }

Specify transmission type

##### We will do 2 tests here, the actual implementation of the types are in RETR and STOR which we can test later

##### 1. Test with valid type

1. Login with USER: AAA
2. TYPE with argument A or B or C

```
	USER AAA
	FROM SERVER: !AAA logged in
	TYPE A
	FROM SERVER: +Using Ascii mode
```

##### 2. Test with invalid type

1. Login with USER: AAA
2. TYPE with argument K

```
	USER AAA
	FROM SERVER: !AAA logged in
	TYPE K
	FROM SERVER: -Type not valid
```

# LIST { F | V } <directory>

lists the files in directory, lists files in default if no directory argument. Must have received "!" to use this command.

1. Log in with USER: AAA 
2. You are now logged into the default directory (src folder)
3. try the following cases

##### We will test 4 cases:

##### 1. Standard list format
```
	LIST F
	FROM SERVER: +/home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java: AAA 
	TCPConnectionInstance.java
	Types.class
	TCPServer.class
	TCPServer.java
	Types.java
	TCPConnectionInstance.class
	TCPClient.java
	Accounts
```

##### 2. Verbose list format - Name, Size, Last Modified, 
```
	LIST V
	FROM SERVER: +/home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java: AAA 
	TCPConnectionInstance.java     21781                Mon Aug 20 15:42:06 NZST 2018
	Types.class                    801                  Mon Aug 20 04:10:22 NZST 2018
	TCPServer.class                613                  Mon Aug 20 15:42:14 NZST 2018
	TCPServer.java                 652                  Mon Aug 20 03:36:50 NZST 2018
	Types.java                     53                   Mon Aug 06 18:09:42 NZST 2018
	TCPConnectionInstance.class    10978                Mon Aug 20 15:42:15 NZST 2018
	TCPClient.java                 8716                 Mon Aug 20 01:16:48 NZST 2018
	Accounts                       4096                 Mon Aug 20 04:16:44 NZST 2018
```

##### 3. invalid list format
```
	LIST O
	FROM SERVER: -invalid listing type
```

##### 4. invalid file directory. Copy and paste an absolute file directory from your device and replace "invalid/invalid/invalid" to list for that directory
```
	LIST V invalid/invalid/invalid
	FROM SERVER: -invalid directory
```

# CDIR <new-directory>

Changes directory of server side.

I will test with my own directory but the marker might want to use their own for this to work. If the user is not logged into an admin account the first time then 
the user will need to enter admin username and password everytime he/she tries to use CDIR. This is behaves windows in the university.

##### We will test 123 test cases:

##### 1. Provide invalid directory:

1. Log in with non-admin account USER: AAA
2. provide a path to a file as an argument or an invalid path
```
	USER AAA
	FROM SERVER: !AAA logged in
	
	CDIR /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/TCPClient.java
	FROM SERVER: -invalid new directory
```

##### 2. Change directory when already logged into admin account:
1. Log in with USER: Admin ACCT: adminA PASS: adminP
2. CDIR to new directory like below but with your own path
3. use LIST F to see that the directory has changeds
```
	USER Admin
	FROM SERVER: +User-id valid, send account and password
	ACCT adminA
	FROM SERVER: +adminA valid, send password
	PASS adminP
	FROM SERVER: !valid password, logged-in

	CDIR /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts
	FROM SERVER: !Changed working dir to /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts

	LIST F
	FROM SERVER: +/home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts: Admin 
	Accounts.txt
	loginResponse.java
	AccountsManager.java
	loginResponse.class
	AccountsManager.class
```
##### 3. Change directory when not already logged into admin account:

1. Log in with non-admin account USER: AAA
2. Change directory
3. Provide admin account with ACCT: adminA, PASS: adminP
4. LIST F again to verify that the directory has been changed
```
	USER AAA
	FROM SERVER: !AAA logged in
	
	CDIR /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts
	FROM SERVER: +directory ok, send admin account/password
	
	ACCT adminA
	FROM SERVER: +correct username, please send password for potential admin access
	PASS adminP
	FROM SERVER: !Admin account verified, Changed working dir to /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts
```
##### 4. Change directory when not already logged into admin account, but provide an account that has no admin access:

1. Log in with non-admin account USER: AAA
2. Change directory
3. Provide non-admin account with ACCT: hdin898, PASS: 12345

```
	USER AAA
	FROM SERVER: !AAA logged in
	
	CDIR /home/chris/Desktop/rfc-913/COMPSYS725hdin898/src/main/java/Accounts
	FROM SERVER: +directory ok, send admin account/password
	
	ACCT hdin898
	FROM SERVER: +correct username, please send password for potential admin access
	PASS 12345
	FROM SERVER: -correct credentials but not admin access, please send account/username of admin
```
I did not implement the server to immediately respond with "correct username but not admin access" or "correct password but not admin access" because that is how it's like in windows. 
You always have to provide username and password before you are given information or error details.

# KILL <filename>
Deletes the specified file in the current directory of the server.

##### We will do 3 tests:

##### 1. Try delete the textKILL.txt file
1. login with USER: AAA
2. Make sure there is a testKILL.txt file in the src folder, if there isn't then create one. (it should be there by default)
3. you can delete this file with the following KILL command
4. you can verify that the file has been deleted by checking in the src folder again
```
	KILL testKILL.txt
	FROM SERVER: +testKILL.txt deleted
```

##### 2. Try delete an invalid file
1. login with USER: AAA
2. KILL a non-existent file like below
``` 
	KILL randomFile.txt
	FROM SERVER: -Not deleted because file not found
```
##### 3. Try delete a non-empty directory
1. login with USER: AAA
2. Make sure there is a folder called testKILLfolder and there are contents inside the folder  (it should be there by default)
3. KILL a directory recursively like below
```
	KILL testKILLfolder
	FROM SERVER: +testKILLfolder deleted
```

# NAME <filename> and TOBE <filename>

##### We will try 3 tests:

##### 1. Try rename the textNAME.txt file	
1. login with USER: AAA
2. Make sure there is a testNAME.txt file in the src folder, if there isn't then create one. (it should be there by default)
3. you can specify this file for renaming with the NAME command
4. next rename the file to the new name with the following TOBE command
5. you can verify that the file has been renamed by checking in the src folder again

```
	NAME testNAME.txt
	FROM SERVER: +File exists
	TOBE testNAME1.txt
	FROM SERVER: +testNAME.txt renamed to testNAME1.txt
```

##### 2. Try re-name a non-existent file
1. login with USER: AAA
2. NAME a non-existent file like below
``` 
	NAME randomeFile.txt  
	FROM SERVER: -Can't find randomeFile.txt NAME command is aborted, don't send TOBE
```

##### 3. Try re-name a file to an existing file
1. login with USER: AAA
2. Find the testNAME1.txt file that was renamed before from src folder
3. NAME and TOBE an existing file like below
```
	NAME testNAME1.txt
	FROM SERVER: +File exists
	TOBE TCPServer.java
	FROM SERVER: -File wasn't renamed because already exists, use LIST command to check for valid file names
```
	
# DONE
Stops the particular thread on server side and closes connection. Also exists the process on client side and closes connection. Change login status to "no" when logging off

##### we will try 1 test case

1. Login with USER: Chris ACCT: hdin898 PASS: 12345
2. type the following DONE command and the process on client side should end
3. check in Account.txt file in the src/Accounts directory to see that the login status of user Chris is "no"

```
	DONE
	FROM SERVER: +closing connection
```
# RETR <filename>

Asks for a file to receive and stores it in the client side's current directory, have to be logged in to user this command
Binary is implemented with DataInputStream and Ascii is implemented with BufferedReader.

##### We will try 4 test cases

##### 1. retrieve an invalid file
1. login with USER: AAA
2. RETR a random non-existent file
```
	RETR randomFile.txt
	FROM SERVER: -File doesn't exist or is directory
```

##### 2. retrieve a valid file with default binary mode
1. login with USER: AAA
2. create a new testRETR.txt file in the src folder on server side (NOT there by  default)
3. send RETR command like below
4. after the folow commands are ran then check the src folder on client side and make sure the contents in the testRETR.txt is the same as the server side
```
	RETR testRETR.txt
	FROM SERVER: 1192
	receiving file...
	finished receiving file.
```
##### 3. retrieve a binary file after switching to Ascii mode
1. login with USER: AAA
2. create a random PNG file or PDF file or any binary file on server side src folder (NOT there by default)
3. use TYPE A to change type to Ascii
4. use RETR yourimage.png to retrieve the PNG file or whatever binary file
5. observe the image is now in your client side src folder without corruption
```
	USER AAA
	FROM SERVER: !AAA logged in
	TYPE A
	FROM SERVER: +Using Ascii mode
	RETR yourimage.png
	FROM SERVER: -Cannot transfer binary file in ASCII mode, please use TYPE command to change
```

##### 4. retrieve a file in any mode with artificial delay so the client side would time out (this timeout timer restarts every time a byte is received, test with a large binary file to proove)

1. Open up TCPConnectionInstance.java
2. In the 17th line change the DEBUGTIMEOUT variable to true (this introduces a 4 sec delay and our tiemout is 2 secs so the client will time out)
3. re-compile with javac TCPServer.java
4. re-run with java TCPServer
5. Login with USER: AAA
6. send RETR command like below
7. observe client side socket closes and ends process
8. observer server side also closes socket and ends thread run
9. Don't forget to change DEBUGTIMEOUT back to false
```
	RETR testRETR.txt
	FROM SERVER: 1192
	receiving file...
	Transfer timeout, closing socket
```


##### 2. retrieve an invalid file

# STOR {NEW | OLD | APP} <filename>

Asks for the server to store a file in its current directory, have to be logged in to user this command

The ascii and binary tests are the same as RETR command. Can try to change type with TYPE command to ascii and try store a binary file (it will not work).

##### We will try 5 test cases:

##### 1. test NEW command
1. Login with USER: AAA
2. create a new testRETRNEW.txt file in the src folder on client side (NOT there by default)
3. enter some text into the testRETRNEW.txt file that you just created e.g. abcdefg
4. run following command to STOR the file into server side
5. verify that the contents of the files are the same
6. run the same command again for existing file on server side when NEW type and see error message like below
```
	STOR NEW testSTORNEW.txt
	FROM SERVER: +File does not exist, will create new file
	FROM SERVER: +ok, waiting for file
	FROM SERVER: +Saved testSTORNEW.txt

	STOR NEW testSTORNEW.txt
	FROM SERVER: -File exists, but system doesn't support generations
```
##### 2. test OLD command
1. Login with USER: AAA
2. create a new testRETROLD.txt file in the src folder on client side (NOT there by default)
3. enter some text into the testRETROLD.txt file that you just created e.g. abcdefg
4. run following command to STOR the file into server side
5. verify that the contents of the files are the same
6. change the contents to something else on the client side e.g. hijklmn
7. run the same command again for existing file on server side when OLD type
8. verify that the file on the server side has been overwritten and only hijklmn is in the file
```
	STOR OLD testSTOROLD.txt
	FROM SERVER: +will create new file
	FROM SERVER: +ok, waiting for file
	FROM SERVER: +Saved testSTOROLD.txt

	STOR OLD testSTOROLD.txt
	FROM SERVER: +Will write over old file
	FROM SERVER: +ok, waiting for file
	FROM SERVER: +Saved testSTOROLD.txt
```

##### 3. test APP command
1. Login with USER: AAA
2. create a new testRETRAPP.txt file in the src folder on client side (NOT there by default)
3. enter some text into the testRETRAPP.txt file that you just created e.g. abcdefg
4. run following command to STOR the file into server side
5. verify that the contents of the files are the same
6. change the contents to something else on the client side e.g. hijklmn
7. run the same command again for existing file on server side when APP type
8. verify that the file on the server side has been concatenated with the file on the client side
```
	STOR APP testSTORAPP.txt
	FROM SERVER: +will create file
	FROM SERVER: +ok, waiting for file
	FROM SERVER: +Saved testSTORAPP.txt

	STOR APP testSTORAPP.txt
	FROM SERVER: +Will append to file
	FROM SERVER: +ok, waiting for file
	FROM SERVER: +Saved testSTORAPP.txt
```

##### 4. test invalid operation type
1. Login with USER: AAA
2. Run the following 
```
	STOR ABC testSTORAPP.txt
	FROM SERVER: -invalid storage type
```

##### 5. test invalid file
1. Login with USER: AAA
2. Run the following
```
	STOR NEW randomFile.txt
	Invalid file, specify new file
```

# GENERIC TESTS

##### 1. For some commands you have to be logged in to run them, without a "!" from the server you will not be able to run them e.g.
```
	RETR abc.txt
	Not logged in, please login before sending other commands
```

##### 2. When you enter an unrecognized command code e.g.
```
	ABCD 
	FROM SERVER: Invalid CODE
```

##### 3. For when commands are expecting arguments but you don't provide them
```
	USER Chris
	FROM SERVER: !Chris logged in
	LIST
	FROM SERVER: -missing argument
```